# --- !Ups

CREATE TABLE department(
  id UUID PRIMARY KEY,
  name VARCHAR (64) UNIQUE NOT NULL,
  date_created bigint NOT NULL,
  date_updated bigint
);

CREATE TABLE employee(
  id UUID PRIMARY KEY,
  first_name VARCHAR (64) NOT NULL,
  last_name VARCHAR (64) NOT NULL,
  date_created bigint NOT NULL,
  date_updated bigint,
  department_id UUID NOT NULL,
  CONSTRAINT department_fk FOREIGN KEY (department_id)
  REFERENCES department (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE CASCADE
);

# --- !Downs
DROP TABLE IF EXISTS department CASCADE;
DROP TABLE IF EXISTS employee CASCADE;
