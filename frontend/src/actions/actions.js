import { createActions } from "redux-actions";
import Api from '../utils/api';
import { toastr } from "react-redux-toastr";
import toastrOptions from "../utils/toastr";

const actionCreators = createActions({
    departments: {
        request: undefined,
        success: undefined,
        failure: undefined,
        deleteDepartment: id => ({ id }),
        saveDepartment: undefined,
        fetchDepartmnet: undefined,
        search: searchQuery => ({ searchQuery })
    },
    spinner: {
        show: undefined,
        hide: undefined
    },
    modals: {
        show: modal => ({ modal }),
        hide: id => ({ id })
    }
});

actionCreators.departments.fetch = () => {
    return (dispatch) => {
        dispatch(actionCreators.departments.request());

        return Api.getDepartments().then(
            response => {
                dispatch(actionCreators.departments.success({ departments: response.data }));
            },
            error => {
                toastr.error('Error', error, toastrOptions); //todo can be curried
            });
    };
};

export default actionCreators;
