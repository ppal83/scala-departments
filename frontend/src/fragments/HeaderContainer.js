import * as React from "react";
import { connect } from "react-redux";
import { Link, withRouter } from "react-router-dom";
import { Button, FormControl, FormGroup, Navbar } from "react-bootstrap";

import actionCreators from "../../actions/actions";


const HeaderContainer = (props) => {
    const searchIsRendered = props.location.pathname === "/tasksboard";
    const isLoginPage = props.location.pathname === "/login";
    const isAuthenticated = !!props.user; // todo find how to get it clear

    const { searchQuery, dispatch } = props;

    const handleSubmit = (e) => {
        e.preventDefault();
        const value = e.target.elements[0].value;
        dispatch(actionCreators.linesList.search(value))
    };

    const handleReset = (e) => {
        e.preventDefault();
        e.target.form.reset();
        dispatch(actionCreators.linesList.search(""))
    };

    const handleLogout = () => {
        dispatch(actionCreators.login.logout());
    };

    return (
        <Navbar fluid>
            <Navbar.Header>
                <Navbar.Brand>
                    <a href="https://www.aimprosoft.com/">
                        <img src="/aimprosoft_logo.png" alt="logo" />
                    </a>
                </Navbar.Brand>
                <Navbar.Toggle />
            </Navbar.Header>

            <Navbar.Collapse>
                {searchIsRendered &&
                <Navbar.Form pullLeft>
                    <form id="search-form" onSubmit={handleSubmit}>
                        <FormGroup>
                            <FormControl type="text" placeholder="Search" defaultValue={searchQuery} />
                        </FormGroup>
                        <Button type="submit">Filter</Button>
                        <Button type="reset" onClick={handleReset}>Reset</Button>
                    </form>
                </Navbar.Form>
                }

                <div className="pull-right navbar-login">
                    {!isAuthenticated && !isLoginPage &&
                    <Link to={{ pathname: "/login", state: { from: props.location } }} className="btn btn-default">Login</Link>
                    }
                    {isAuthenticated && !isLoginPage &&
                    <div>
                        <Navbar.Text>
                            Logged in as: { props.user.sub }
                        </Navbar.Text>
                        <Button onClick={handleLogout}>Logout</Button>
                    </div>
                    }
                </div>
            </Navbar.Collapse>
        </Navbar>
    );
};

const mapStateToProps = (state) => {
    const { searchQuery }  = state.linesList;
    const { user } = state.login;

    return {
        searchQuery,
        user
    };
};

export default withRouter(connect(mapStateToProps)(HeaderContainer));
