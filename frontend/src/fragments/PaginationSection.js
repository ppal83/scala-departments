import React from 'react';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';

export default function PaginationSection (props) {
    const MAX_PAGES = 5;
    const { path, page = 1, size = 5, total = 6} = props;
    let pagesCount = Math.floor(total / size) + 1;
    pagesCount = pagesCount > MAX_PAGES ? MAX_PAGES : pagesCount;
    console.log("pagesCount = " + pagesCount);
    Array.from(Array(pagesCount).keys()).forEach(i => console.log(i));

    return (
        <div>
            <Pagination aria-label="Page navigation example">
                <PaginationItem>
                    <PaginationLink href="#">First</PaginationLink>
                </PaginationItem>
                <PaginationItem>
                    <PaginationLink href="#">Next</PaginationLink>
                </PaginationItem>
                {Array.from(Array(pagesCount).keys()).map(i => (
                    <PaginationItem key={i + 1}>
                        <PaginationLink href="#">{i + 1}</PaginationLink>
                    </PaginationItem>
                ))}

                {/*<PaginationItem>*/}
                    {/*<PaginationLink href="#">2</PaginationLink>*/}
                {/*</PaginationItem>*/}
                <PaginationItem disabled>
                    <PaginationLink href="#">Previous</PaginationLink>
                </PaginationItem>
                <PaginationItem disabled>
                    <PaginationLink href="#">Last</PaginationLink>
                </PaginationItem>
            </Pagination>
        </div>
    );
}
