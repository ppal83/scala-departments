import {combineReducers} from "redux";
import {combineActions, handleActions} from "redux-actions";
import {reducer as formReducer} from "redux-form";
import {reducer as toastrReducer} from "react-redux-toastr";
import actionCreators from "../actions/actions";

const departments = handleActions({
        [actionCreators.departments.success](state, {payload: {departments}}) {
            return {...state, departments: departments.items, total: departments.total};
        },
        [actionCreators.departments.search](state, {payload: searchQuery}) {
            return {...state, searchQuery};
        }
    }, {departments: [], total: 0}
);

const isFetching = handleActions({
    [combineActions(
        actionCreators.departments.request,
        actionCreators.spinner.show
    )]() {
        return true;
    },
    [combineActions(
        actionCreators.departments.success,
        actionCreators.spinner.hide
    )]() {
        return false;
    }
}, false);

const rootReducer = combineReducers({
    departments,
    isFetching,
    toastr: toastrReducer,
    form: formReducer
});

export default rootReducer;
