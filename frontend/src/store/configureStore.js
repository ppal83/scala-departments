import thunkMiddleware from "redux-thunk";
import {applyMiddleware, createStore} from "redux";
import {composeWithDevTools} from "redux-devtools-extension";
import rootReducer from "../reducers/reducers";

export default function configureStore(initialState) {

    // configure middlewares
    const middlewares = [
        thunkMiddleware
    ];

    // compose enhancers
    const enhancer = composeWithDevTools(
        applyMiddleware(...middlewares)
    );

    // create store
    return createStore(
        rootReducer,
        initialState,
        enhancer
    );

}
