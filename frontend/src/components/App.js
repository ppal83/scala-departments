import React, { Component } from "react";
import { Redirect, Route, Switch } from "react-router-dom";

import "./App.css";
import "react-redux-toastr/lib/css/react-redux-toastr.min.css";
import DepartmentsContainer from './departments/DepartmentsContainer';
import NotFound from '../pages/NotFound';

// import ModalsContainer from "../components/fragments/ModalsContainer";
// import IsFetchingSpinner from "./fragments/IsFetchingSpinner";
// import HeaderContainer from "./fragments/HeaderContainer";
// import NotFound from "./pages/NotFound";
// import actionCreators from "../actions/actions";

class App extends Component {

    render () {
        return (
            <React.Fragment>
                {/*<ModalsContainer />*/}
                {/*<IsFetchingSpinner />*/}
                {/*<HeaderContainer />*/}
                <Switch>
                    <Redirect exact from="/" to="departments" />
                    <Route exact path="/departments" component={DepartmentsContainer} />
                    <Route component={NotFound} />
                </Switch>
            </React.Fragment>
        );
    }
}

export default App;
