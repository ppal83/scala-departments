import React, { Component } from "react";
import * as _ from 'lodash-es';
import { Table, Button } from 'reactstrap';
import { connect } from "react-redux";
import { createSelector } from "reselect";
import actionCreators from "../../actions/actions";
import resolvePageParams from '../../utils/query-params-util';
import PaginationSection from '../../fragments/PaginationSection';

class DepartmentsContainer extends Component {

    componentDidMount() {
        console.log('Mounted');
        console.log(this.props);
        const { dispatch } = this.props;
        dispatch(actionCreators.departments.fetch());
    }

    // componentWillReceiveProps(nextProps) {
    // }

    componentDidUpdate() {
        console.log('Updated');
        console.log(this.props);
    }

    render() {
        console.log("Rendered");
        const { departments } = this.props;

        return (
            <React.Fragment>
                <PaginationSection match={this.props.match}/>
                <Button color="primary">Add department</Button>
                <Table bordered hover responsive className="departments-table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    {departments.map(dept => (
                        <tr key={dept.id}>
                            <th scope="row" className="w-25">{dept.id}</th>
                            <td className="w-50">{dept.name}</td>
                            <td className="w-25">
                                <Button color="primary">Edit</Button>
                                <Button color="danger">Delete</Button>
                            </td>
                        </tr>
                    ))}
                    </tbody>
                </Table>
                <PaginationSection/>
            </React.Fragment>
        );
    }
}

const searchSelector = (state, ownProps) => ({
    total: state.departments.total,
    // locationSearch: ownProps.location.search
});
const pageParamsSelector = createSelector(searchSelector, resolvePageParams);

function mapStateToProps(state, ownProps) {
    const { departments } = state.departments;

    return {
        departments,
        pageParams: pageParamsSelector(state, ownProps)
        // pageParams: pageParamsSelector(state, ownProps)
    }
}

export default connect(mapStateToProps)(DepartmentsContainer);

// Helpers
function pageParamsChanged(thisProps, nextProps) {
    return !_.isEqual(thisProps.pageParams, nextProps.pageParams);
}
