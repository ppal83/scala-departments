import React from "react";
import axios from 'axios';
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import ReduxToastr from "react-redux-toastr";
import { BrowserRouter as Router } from "react-router-dom";

import App from "./components/App";
import configureStore, { history } from "./store/configureStore";
import registerServiceWorker from "./registerServiceWorker";

import 'bootstrap/dist/css/bootstrap.css';

let initialState = {
    departments: {
        departments: [],
        total: 0
    },
    // department: null,
    // employees: {
    //     employees: {},
    //     total: 0,
    // },
    // employee: null,
    isFetching: false
};

const store = configureStore(initialState);

// Axios defaults
axios.defaults.baseURL = process.env.REACT_APP_SERVER_API;

ReactDOM.render(
    <Provider store={store}>
        <Router>
            <React.Fragment>
                <App />
                <ReduxToastr
                    timeOut={3000}
                    newestOnTop={false}
                    preventDuplicates
                    positionClass="toastr-bottom-custom"
                    transitionIn="fadeIn"
                    transitionOut="fadeOut"
                    progressBar/>
            </React.Fragment>
        </Router>
    </Provider>,
    document.getElementById('root')
);

registerServiceWorker();
