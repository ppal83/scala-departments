import React from 'react';
import { Link } from "react-router-dom";

export default function NotFound () {

    return (
        <div className="col-md-3 col-md-offset-5 info-container">
            <h1>Fuck! 404!</h1>
            <p>Gerrarahia, man... I say...</p>
            <img id="img-gerrarahia" src="/gerrarahia.jpg" alt="gerrarahia man"/>
            <div className="pull-right">
                <Link to="/" className="btn btn-default">Back to main</Link>
            </div>
        </div>
    );
}
