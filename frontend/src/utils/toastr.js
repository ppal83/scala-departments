const toastrOptions = {
    width: '700px',
    progressBar: false
};

export default toastrOptions;
