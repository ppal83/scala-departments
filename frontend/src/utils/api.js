import axios from "axios/index";


export const routes = {
    V1: {
        DEPARTMENTS: "/api/v1/departments",
        // DEPARTMENTS_PAGE: "/api/v1/departmentsPage",
        DEPARTMENT: "/api/v1/department"
    }
};

const Api = {
    getDepartments() {
        return axios.get(`${routes.V1.DEPARTMENTS}`);
    },
    // getDepartmentsPage() {
    //     return axios.get(`${routes.V1.DEPARTMENTS_PAGE}?page=0&size=3`);
    // },
    getDepartment(id) {
        return axios.get(`${routes.V1.DEPARTMENT}/${id}`);
    }
};

export default Api;
