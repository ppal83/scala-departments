import { ASC, DESC } from './constants';

const defaults = {
    page: 1,
    size: 5,
    total: 0,
    orderByCol: undefined,
    orderByType: undefined,
    search: undefined
};

export default function resolvePageParams({ total, locationSearch }) {
    let page = getParameterByName('page', locationSearch);
    let size = getParameterByName('size', locationSearch);
    let orderByTypeParam = getParameterByName('orderByType', locationSearch);
    const orderByCol = getParameterByName('orderByCol', locationSearch);
    const search = getParameterByName('search', locationSearch);

    let result = { ...defaults, total };

    if (validatePositiveInteger(page) && validatePositiveInteger(size)) {
        page = parseInt(page, 10);
        size = parseInt(size, 10);
        total = parseInt(total, 10);

        if ((page > 1) && (Math.floor(total / ((page - 1) * size)) == 0)) {
            page = 1;
        }

        result = { ...result, ...{ page, size } };
    }

    // todo validate orderByCol and orderByType

    let orderByType = orderByTypeParam
        ? (orderByTypeParam === ASC) ? ASC : DESC
        : undefined;
    result = {...result, orderByType, orderByCol};

    if (search) {
        result = {...result, search};
    }

    return result;
}

function getParameterByName(name, url) {
    name = name.replace(/[\[\]]/g, '\\$&');
    const regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) {
        return '';
    }
    if (!results[2]) {
        return '';
    }
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function validatePositiveInteger(str) {
    return /^\+?([1-9]\d*)$/.test(str);
}
