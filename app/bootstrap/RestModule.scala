package bootstrap

import com.google.inject.AbstractModule

class RestModule extends AbstractModule {
  override def configure(): Unit = {
    bind(classOf[InitialData]).asEagerSingleton()
  }
}
