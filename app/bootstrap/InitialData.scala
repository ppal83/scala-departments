package bootstrap

import java.util.{Date, UUID}

import com.aimprosoft.dao.{DepartmentsDao, EmployeesDao}
import javax.inject.Inject
import models.IDType.ID
import models.{Department, Employee}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}
import scala.util.Try

private[bootstrap] class InitialData @Inject()(departmentsDao: DepartmentsDao,
                                               employeesDao: EmployeesDao)
                                              (implicit executionContext: ExecutionContext) {

  def init(): Unit = {
    val insertInitialDataFuture = for {
      _ <- departmentsDao.insert(InitialData.departments)
      _ <- employeesDao.insert(InitialData.employees)
    } yield ()

    Try(Await.result(insertInitialDataFuture, Duration.Inf))
  }

  init()
}

private[bootstrap] object InitialData {

  val productionDeptId: Option[ID] = Some(UUID.randomUUID())
  val researchAndDevelopmentDeptId:  Option[ID] = Some(UUID.randomUUID())
  val purchasingDeptId:  Option[ID] = Some(UUID.randomUUID())
  val marketingDeptId:  Option[ID] = Some(UUID.randomUUID())
  val humanResourceManagementDeptId:  Option[ID] = Some(UUID.randomUUID())
  val accountingAndFinanceDeptId:  Option[ID] = Some(UUID.randomUUID())

  val now = Some(new Date())

  def departments = Seq(
    Department(productionDeptId, "Production", now, now),
    Department(researchAndDevelopmentDeptId, "Research And Development", now, now),
    Department(purchasingDeptId, "Purchasing", now, now),
    Department(marketingDeptId, "Marketing", now, now),
    Department(humanResourceManagementDeptId, "Human Resource", now, now),
    Department(accountingAndFinanceDeptId, "Accounting And Finance", now, now)
  )

  val employeeToFindById: Option[ID] = Some(UUID.randomUUID())

  def employees = Seq(
    Employee(employeeToFindById, "Petr", "Poroshenko", now, now, marketingDeptId.get),
    Employee(Some(UUID.randomUUID()), "Vladimir", "Groysman", now, now, humanResourceManagementDeptId.get),
    Employee(Some(UUID.randomUUID()), "Arsen", "Avakov", now, now, productionDeptId.get),
    Employee(Some(UUID.randomUUID()), "Pavlo", "Klimkin", now, now, purchasingDeptId.get),
    Employee(Some(UUID.randomUUID()), "Arseniy", "Yatsenyuk", now, now, researchAndDevelopmentDeptId.get),
    Employee(Some(UUID.randomUUID()), "Yulia", "Timoshenko", now, now, accountingAndFinanceDeptId.get),
    Employee(Some(UUID.randomUUID()), "Anatoliy", "Gritsenko", now, now, productionDeptId.get),
    Employee(Some(UUID.randomUUID()), "Oleg", "Lyashko", now, now, marketingDeptId.get),
    Employee(Some(UUID.randomUUID()), "Vladimir", "Klitchko", now, now, researchAndDevelopmentDeptId.get),
    Employee(Some(UUID.randomUUID()), "Stepan", "Poltorak", now, now, productionDeptId.get),
    Employee(Some(UUID.randomUUID()), "Andrey", "Reva", now, now, humanResourceManagementDeptId.get)
  )

}
