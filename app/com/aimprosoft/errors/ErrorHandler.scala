package com.aimprosoft.errors

import javax.inject.Singleton
import models.Error
import play.api.Logger
import play.api.http.HttpErrorHandler
import play.api.libs.json.{Json, OWrites}
import play.api.mvc.Results._
import play.api.mvc._

import scala.concurrent._

@Singleton
class ErrorHandler extends HttpErrorHandler {

  implicit val errorWrites: OWrites[Error] = Json.writes[Error]

  def onClientError(request: RequestHeader, statusCode: Int, message: String): Future[Result] = {
    Logger.error(message)
    Future.successful(
      Status(statusCode) {
        Json.toJson(Error(statusCode, message, request.path))
      }
    )
  }

  def onServerError(request: RequestHeader, exception: Throwable): Future[Result] = {
    Logger.error(exception.getMessage, exception)
    Future.successful(
      InternalServerError {
        Json.toJson(Error(500, exception.getMessage, request.path))
      }
    )
  }

}
