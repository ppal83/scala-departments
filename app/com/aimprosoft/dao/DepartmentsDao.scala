package com.aimprosoft.dao

import javax.inject.{Inject, Singleton}
import models.Department
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.ExecutionContext

trait DepartmentsComponent extends BaseTableProvider {
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api._

  class DepartmentsTable(tag: Tag) extends BaseTable[Department](tag, "department") {

    def name = column[String]("name")
    def * = (id.?, name, dateCreated.?, dateUpdated.?).mapTo[Department]
  }

  lazy val departments = TableQuery[DepartmentsTable]

}

@Singleton()
class DepartmentsDao @Inject()(override protected val dbConfigProvider: DatabaseConfigProvider)
                              (implicit executionContext: ExecutionContext)
  extends BaseDao[Department](dbConfigProvider) with DepartmentsComponent {

  import profile.api._

  lazy val entities = departments

  def searchFor(search: String): Query[DepartmentsTable, Department, Seq] = {
    entities.filter(_.name.toLowerCase like search.toLowerCase)
  }

}
