package com.aimprosoft.dao

import java.util.Date

import models.IDType.ID
import models._
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}


trait BaseTableProvider {
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api._

  abstract class BaseTable[T <: BaseEntity](tag: Tag, tableName: String) extends Table[T](tag, tableName) {
    implicit val dateColumnTypeConv: BaseColumnType[Date] = MappedColumnType.base[Date, Long](_.getTime, new Date(_))

    def id = column[ID]("id", O.PrimaryKey, O.Unique)
    def dateCreated = column[Date]("date_created")
    def dateUpdated = column[Date]("date_updated")
  }

}

abstract class BaseDao[T <: BaseEntity](protected val dbConfigProvider: DatabaseConfigProvider)
                                       (implicit executionContext: ExecutionContext)
  extends BaseTableProvider with HasDatabaseConfigProvider[JdbcProfile] {

  import profile.api._

  val entities: TableQuery[_ <: BaseTable[T]]

  def insert(entity: T): Future[Unit] = {
    db.run((entities += entity).transactionally).map(_ => ())
  }

  def insert(entitiesToAdd: Seq[T]): Future[Option[Int]] = {
    db.run((entities ++= entitiesToAdd).transactionally)
  }

  def save(entity: T): Future[Unit] = {
    db.run(entities.insertOrUpdate(entity).transactionally).map(_ => ())
  }

  def count(): Future[Int] = {
    db.run(entities.length.result)
  }

  def getAll: Future[Iterable[T]] = {
    db.run(entities.result)
  }

  def searchFor(search: String): Query[_ <: BaseTable[T], T, Seq] // to be implemented in a subclass

  def count(search: String): Future[Int] = {
    db.run(searchFor(search).length.result)
  }

  def getPage(pageRequest: PageRequest): Future[PageResult[T]] = {
    var PageRequest(page: Int, size: Int, search: String) = pageRequest
    search = if (search != "%") s"%$search%" else search
    var query = searchFor(search)
    if (page >= 0 && size >= 0) query = query.drop(page * size).take(size)

    for {
      items <- db.run(query.result)
      total <- count(search)
    } yield PageResult(items, total)
  }

  def getById(id: ID): Future[Option[T]] = {
    db.run(entities.filter(_.id === id).result.headOption)
  }

  def deleteAll(): Future[Unit] = {
    db.run(entities.delete.transactionally).map(_ => ())
  }

  def deleteById(id: ID): Future[Int] = {
    db.run(entities.filter(_.id === id).delete.transactionally)
  }

}
