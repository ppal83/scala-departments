package com.aimprosoft.dao

import javax.inject.{Inject, Singleton}
import models.Employee
import models.IDType.ID
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.ExecutionContext

trait EmployeesComponent extends DepartmentsComponent with BaseTableProvider {
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api._

  class EmployeesTable(tag: Tag) extends BaseTable[Employee](tag, "employee") {

    def firstname = column[String]("first_name")
    def lastname = column[String]("last_name")
    def departmentId = column[ID]("department_id")
    def * = (id.?, firstname, lastname, dateCreated.?, dateUpdated.?, departmentId).mapTo[Employee]

    def departments_fk = foreignKey("department_fk", departmentId, departments)(_.id, onDelete=ForeignKeyAction.Cascade)
  }

  lazy val employees = TableQuery[EmployeesTable]
}

@Singleton()
class EmployeesDao @Inject()(override protected val dbConfigProvider: DatabaseConfigProvider)
                            (implicit executionContext: ExecutionContext)
  extends BaseDao[Employee](dbConfigProvider) with EmployeesComponent {

  import profile.api._

  lazy val entities = employees

  def searchFor(search: String): Query[EmployeesTable, Employee, Seq] = {
    entities.filter { entity =>
      entity.firstname.toLowerCase.like(search.toLowerCase) || entity.lastname.toLowerCase.like(search.toLowerCase)
    }
  }

}
