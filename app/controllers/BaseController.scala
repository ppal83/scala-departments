package controllers

import converters._
import models.Error
import play.api.libs.json.Json
import play.api.mvc._

class BaseController (cc: ControllerComponents) extends AbstractController(cc)
  with play.api.i18n.I18nSupport {

  // Helpers
  def notFoundResult(request: Request[AnyContent], message: String): Result = {
    NotFound {
      Json.toJson {
        Error(NOT_FOUND, message, request.path)
      }
    }
  }

}
