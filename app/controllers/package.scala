import java.util.{Date, UUID}

import models.{Department, Employee, Error, PageResult}
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Json, OWrites, Reads}

package object converters {

  implicit val departmentWrites: OWrites[Department] = Json.writes[Department]
  implicit val employeeWrites: OWrites[Employee] = Json.writes[Employee]
  implicit val departmentPageResultWrites: OWrites[PageResult[Department]] = Json.writes[PageResult[Department]]
  implicit val employeePageResultWrites: OWrites[PageResult[Employee]] = Json.writes[PageResult[Employee]]
  implicit val errorWrites: OWrites[Error] = Json.writes[Error]

  implicit val departmentReads: Reads[Department] = (
      (JsPath \ "id").readNullableWithDefault(Option(UUID.randomUUID())) and
      (JsPath \ "name").read[String] and
      (JsPath \ "dateCreated").readNullableWithDefault(Option(new Date())) and
      (JsPath \ "dateUpdated").readNullableWithDefault(Option(new Date()))
    )(Department.apply _)

  implicit val employeeReads: Reads[Employee] = (
    (JsPath \ "id").readNullableWithDefault(Option(UUID.randomUUID())) and
      (JsPath \ "firstname").read[String] and
      (JsPath \ "lastname").read[String] and
      (JsPath \ "dateCreated").readNullableWithDefault(Option(new Date())) and
      (JsPath \ "dateUpdated").readNullableWithDefault(Option(new Date())) and
      (JsPath \ "departmentId").read[UUID]
    )(Employee.apply _)


}
