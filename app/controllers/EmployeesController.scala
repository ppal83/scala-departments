package controllers

import com.aimprosoft.dao.EmployeesDao
import converters._
import javax.inject.{Inject, Singleton}
import models.IDType.ID
import models.{Employee, PageRequest}
import play.api.i18n.Messages
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.ExecutionContext

@Singleton
class EmployeesController @Inject()(cc: ControllerComponents, employeesDao: EmployeesDao)
                                   (implicit executionContext: ExecutionContext) extends BaseController(cc) {

  val NotFoundKey = "employee.not.found"

  def employees(page: Int, size: Int, search: String): Action[AnyContent] = Action.async {
    implicit request =>
      val pageRequest = PageRequest(page, size, search)
      employeesDao.getPage(pageRequest).map(res => Ok(Json.toJson(res)))
  }

  def employee(id: ID): Action[AnyContent] = Action.async {
    implicit request =>
      employeesDao.getById(id).map {
        case Some(dept) => Ok(Json.toJson(dept))
        case None => notFoundResult(request, Messages(NotFoundKey, id.toString))
      }
  }

  def deleteEmployee(id: ID): Action[AnyContent] = Action.async {
    implicit request =>
      employeesDao.deleteById(id).map {
        case 1 => NoContent
        case _ => notFoundResult(request, Messages(NotFoundKey, id.toString))
      }
  }

  def saveEmployee: Action[AnyContent] = Action {
    implicit request =>
      val json = request.body.asJson.get
      val employee = json.as[Employee]
      employeesDao.save(employee)
      Ok
  }

}
