package controllers

import com.aimprosoft.dao.DepartmentsDao
import converters._
import javax.inject._
import models.IDType.ID
import models._
import play.api.i18n.Messages
import play.api.libs.json._
import play.api.mvc._

import scala.concurrent.ExecutionContext

@Singleton
class DepartmentsController @Inject()(cc: ControllerComponents, departmentsDao: DepartmentsDao)
                                     (implicit executionContext: ExecutionContext) extends BaseController(cc) {

  val NotFoundKey = "department.not.found"

  def departments(page: Int, size: Int, search: String): Action[AnyContent] = Action.async {
    implicit request =>
      val pageRequest = PageRequest(page, size, search)
      departmentsDao.getPage(pageRequest).map(res => Ok(Json.toJson(res)))
  }

  def department(id: ID): Action[AnyContent] = Action.async {
    implicit request =>
      departmentsDao.getById(id).map {
        case Some(dept) => Ok(Json.toJson(dept))
        case None => notFoundResult(request, Messages(NotFoundKey, id.toString))
      }
  }

  def deleteDepartment(id: ID): Action[AnyContent] = Action.async {
    implicit request =>
      departmentsDao.deleteById(id).map {
        case 1 => NoContent
        case _ => notFoundResult(request, Messages(NotFoundKey, id.toString))
      }
  }

  def saveDepartment: Action[AnyContent] = Action {
    implicit request =>
      val json = request.body.asJson.get
      val department = json.as[Department]
      departmentsDao.save(department)
      Ok
  }

}
