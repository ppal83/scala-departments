package models

import java.util.{Date, UUID}

import models.IDType._

object IDType {
  type ID = UUID
}

// Entities
trait BaseEntity {
  val id: Option[ID]
}

case class Department(id: Option[ID] = None,
                      name: String,
                      dateCreated: Option[Date] = None,
                      dateUpdated: Option[Date] = None) extends BaseEntity

case class Employee(id: Option[ID] = None,
                    firstname: String,
                    lastname: String,
                    dateCreated: Option[Date] = None,
                    dateUpdated: Option[Date] = None,
                    departmentId: ID) extends BaseEntity

case class PageRequest(page: Int = -1, pageSize: Int = -1, search: String = "%")
case class PageResult[T <: BaseEntity](items: Seq[T], total: Long)

case class Error(status: Int, message: String, path: String)
