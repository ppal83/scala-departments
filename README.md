## Scala 'Departments' educational

'Departments and employees' educational project, comprising REST API written on Scala and frontend client written using ReactJS

### Used technologies
- [Play](https://www.playframework.com/) - high velocity web framework for Java and Scala
- [Slick](http://slick.lightbend.com/) - modern database query and access library for Scala
- [React](https://reactjs.org/) - a JavaScript library for building user interfaces

### DB requirements
You should have PostgreSQL installed and running with DB created as follows: *jdbc:postgresql://localhost:5432/scala_departments* 