import sbt.Keys.libraryDependencies

name := "rest-ops-be-play-slick"

version := "2.6.x"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"

resolvers += "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/"

scalaVersion := "2.12.6"

crossScalaVersions := Seq("2.11.12", "2.12.4")

libraryDependencies ++= Seq(
  guice,
  "com.typesafe.play" %% "play-slick" % "3.0.3",
  "com.typesafe.play" %% "play-slick-evolutions" % "3.0.3",
  "org.postgresql" % "postgresql" % "42.2.4",
  "com.h2database" % "h2" % "1.4.197" % Test,
  "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.0" % Test
)

libraryDependencies += evolutions

//libraryDependencies += specs2 % Test

//libraryDependencies += jdbc % Test

unmanagedResourceDirectories in Test += baseDirectory( _ /"target/web/public/test" ).value

javaOptions in Test += "-Dconfig.file=conf/application.test.conf"
